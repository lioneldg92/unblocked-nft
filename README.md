# Deployer Address - (Unblocked)
0x4bD23e98d0D492536F2e84630aDEbE2Fd01AFBc2

# Set up
In root:
```
$ npm install
```

# To compile and deploy the smart-contract
Make sure you have .env from env.example
```
npx hardhat compile
npx hardhat run scripts/deploy.js --network ropsten
```

Get the smart-contract address and flag it into .env file or directly into scripts/mint_nft.js

# To mint from Unblocked account
```
node scripts/mint_nft.js
```

### CID metadata - 
Yosra - QmaaSt6xLUm7LBCYBNG3UVKsdXW7RMw4ghAhcwrE3vStZj  
Luk - QmWhkzicfnqFCoiLDmWbRoWpWKGjFdKqzVrJhyu6LDznfY  
Patrice - QmQoFpP8yeWpf8M9c1qoeV2B6DPMVRm71hdpokeNzfzXpo  
Léo - QmZm2rHGiszpuGCS5aGbJ3R7nRRvhcWxEdZrHYWskjBPCK  
Himran - QmUQqPvBZ4gWVrAzinngVh5tzkrYtBQxjaACfXGZNqdFiJ  
Daniel - QmSksQT82MJrUART8wHkNiruQmmcCr93PQ17qxpv3WewEc  

