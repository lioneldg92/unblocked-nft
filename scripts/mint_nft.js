require("dotenv").config()
const { createAlchemyWeb3 } = require("@alch/alchemy-web3")

const API_URL = process.env.API_URL;
const PUBLIC_KEY = process.env.PUBLIC_KEY;
const PRIVATE_KEY = process.env.PRIVATE_KEY;

const web3 = createAlchemyWeb3(API_URL)

const contract = require("../artifacts/contracts/MyNFT.sol/MyNFT.json")

// Ropsten
const contractAddress = process.env.SC_ADDRESS || "0x16B409D289f58124cd8d41fDFF038C74BFA242cE" // mint on last smart-contrat 

const nftContract = new web3.eth.Contract(contract.abi, contractAddress)

async function mintNFT(tokenURI) {
  const nonce = await web3.eth.getTransactionCount(PUBLIC_KEY, "latest") //get latest nonce
  
  //the transaction
  const tx = {
    from: PUBLIC_KEY,
    to: contractAddress,
    nonce: nonce,
    gas: 500000,
    data: nftContract.methods.mintNFT(PUBLIC_KEY, tokenURI).encodeABI(),
  }
  
  const signPromise = web3.eth.accounts.signTransaction(tx, PRIVATE_KEY)

  signPromise
  .then(async (signedTx) => {
    web3.eth.sendSignedTransaction(
      signedTx.rawTransaction,
      function (err, hash) {
        if (!err) {
          console.log(
            "The hash of your transaction is: ",
            hash,
            "\nCheck Alchemy's Mempool to view the status of your transaction!"
          )
        } else {
          console.log(
            "Something went wrong when submitting your transaction:",
            err
          )
        }
      }
    )
  })
  .catch((err) => {
    console.log("Promise failed:", err)
  })
}


// Yosra 
mintNFT("https://gateway.pinata.cloud/ipfs/QmaaSt6xLUm7LBCYBNG3UVKsdXW7RMw4ghAhcwrE3vStZj");
// Luk
// mintNFT("https://gateway.pinata.cloud/ipfs/QmWhkzicfnqFCoiLDmWbRoWpWKGjFdKqzVrJhyu6LDznfY");
// Patrice
// mintNFT("https://gateway.pinata.cloud/ipfs/QmQoFpP8yeWpf8M9c1qoeV2B6DPMVRm71hdpokeNzfzXpo");
// Léo 
// mintNFT("https://gateway.pinata.cloud/ipfs/QmZm2rHGiszpuGCS5aGbJ3R7nRRvhcWxEdZrHYWskjBPCK");
// Himran 
// mintNFT("https://gateway.pinata.cloud/ipfs/QmUQqPvBZ4gWVrAzinngVh5tzkrYtBQxjaACfXGZNqdFiJ");
// Daniel 
// mintNFT("https://gateway.pinata.cloud/ipfs/QmSksQT82MJrUART8wHkNiruQmmcCr93PQ17qxpv3WewEc");


